import BasePage from 'src/pages/BasePage';

interface IPageElements {
    usernameInput: WebdriverIO.Element;
    passwordInput: WebdriverIO.Element;
    logInBtn: WebdriverIO.Element;
    statusElement: WebdriverIO.Element;
}

class LoginPage extends BasePage {

    constructor() {
        super('/login');
    }
    
    get elements(): IPageElements {
        return {
            usernameInput: $('#user_login'),
            passwordInput: $('#user_pass'),
            logInBtn: $('#wp-submit'),
            statusElement: $('#status')
        }
    }

    loginWithCredentials(username: string, password: string): void {
        this.elements.usernameInput.setValue(username);
        this.elements.passwordInput.setValue(password);
        this.elements.logInBtn.click();
    }
}

export default new LoginPage();
