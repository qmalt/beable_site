export default class BasePage {
    constructor (protected path: string) {
    }

    public navigateTo(): void {
        return browser.url(this.path);
    }
}
