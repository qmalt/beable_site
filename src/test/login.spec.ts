import {expect} from 'chai';
import LoginPage from 'src/pages/LogInPage';

describe('Login page', () => {
    it('displays message with invalid credentials', () => {
        LoginPage.navigateTo();
        LoginPage.loginWithCredentials('test', 'test');

        expect(LoginPage.elements.statusElement.getText()).to.include('Your username is invalid!');
    });

    it('should allow access with correct credentials', () => {
        LoginPage.navigateTo();
        LoginPage.loginWithCredentials('User1', 'Password');

        expect(LoginPage.elements.statusElement.getText()).to.include('You logged into a secure area!');
    });

});



